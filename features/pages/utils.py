import time
from selenium.common.exceptions import WebDriverException

__author__ = 'fomars'


def double_click(driver, element_id):
    SCRIPT = "document.getElementById('{0:s}').dispatchEvent\
                (new MouseEvent\
                ('dblclick', \
                {{'view': window, 'bubbles': true, 'cancelable': true}}))".format(element_id)
    tries = 0
    while tries < 5:
        try:
            driver.execute_script(SCRIPT)
        except WebDriverException:
            time.sleep(0.2)
            tries += 1
        else:
            return
    driver.execute_script(SCRIPT)
from selenium.webdriver.common.by import By

__author__ = 'fomars'


class LoginPageLocators():
    NAME_INPUT = By.ID, 'username'
    PASSWORD_INPUT = By.ID, 'password'


class PlannerTabLocators():
    NAME_INP = By.ID, 'Name'
    NEW = By.CSS_SELECTOR, 'input.btn'
    SAVE_BTN = By.NAME, 'save'

class ProjectEditorLocators():
    SAVE_BTN = By.ID, 'button-1190-btnIconEl'
    NAME_INPUT = By.CSS_SELECTOR, 'input[name="Name"]'
    PREDEC_INPUT = By.CSS_SELECTOR, 'input[name="ext-comp-1026-inputEl"]'
import time

from config import Links
from utils import double_click
from locators import *
from selenium.common.exceptions import StaleElementReferenceException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait


__author__ = 'fomars'


def _is_ajax_active(_browser):
    return _browser.execute_script("""if ((typeof $ != 'undefined') &&
    (typeof $.active != 'undefined')) {return $.active > 0;} else {return true;} """)


def _is_ajax_finished(_browser):
    return _browser.execute_script("""if ((typeof $ != 'undefined') &&
    (typeof $.active != 'undefined')) {return $.active == 0;} else {return true;} """)


def safe_wait_ajax_become_active(driver):
    tries = 0
    while not _is_ajax_active(driver) and tries < 10:
        time.sleep(0.1)
        tries += 1


def ui_wait(wait_function, until, message):
    wait_function(until)


def wait_ajax_finished(driver):
    WebDriverWait(driver, 10).until(_is_ajax_finished)


def wait_for_ajax_call(driver):
    safe_wait_ajax_become_active(driver)
    wait_ajax_finished(driver)


class BasePage(object):
    def __init__(self, url, driver):
        """
        :type driver: WebDriver
        """
        self.url = url
        self.driver = driver

    def go_home(self):
        self.driver.get(self.url)


class LoginPage(BasePage):
    def login(self, username, password):
        self.driver.get(Links.LOGIN_PAGE)
        name = self.driver.find_element(*LoginPageLocators.NAME_INPUT)
        name.send_keys(username)
        self.driver.find_element(*LoginPageLocators.PASSWORD_INPUT).send_keys(password)
        name.submit()


class PlannerTab(BasePage):
    def new_project(self, name):
        # self.driver.get(Links.PLANNER_TAB)
        wait_for_ajax_call(self.driver)
        new_btn = self.driver.find_element(*PlannerTabLocators.NEW)
        new_btn.click()
        name_inp = self.driver.find_element(*PlannerTabLocators.NAME_INP)
        name_inp.send_keys(name)
        self.driver.find_element(*PlannerTabLocators.SAVE_BTN).click()

    def open_project(self, project_name):
        self.driver.find_element_by_partial_link_text(project_name).click()
        wait_for_ajax_call(self.driver)


class Cell(object):
    SELECTOR = '#treeview-1052-table tr:nth-child({0:d}) td:nth-child({1:d})'
    INPUT_LOCATORS = {
        5: ProjectEditorLocators.NAME_INPUT,
        9: ProjectEditorLocators.PREDEC_INPUT
    }

    def __init__(self, driver, row, col):
        """

        :type driver: WebDriver
        :param row:
        :param col:
        """
        self.driver = driver
        self.row = row
        self.col = col
        self.input_locator = self.INPUT_LOCATORS[col]
        self._web_element = None
        self._id = None

    def fill_cell(self, value):
        double_click(self.driver, self.id)

        inp = self.driver.find_element(*self.input_locator)
        try:
            inp.send_keys(value)
        except ElementNotVisibleException:
            self.scroll_to()
            for i in range(5):
                try:
                    inp.send_keys()
                except ElementNotVisibleException:
                    time.sleep(1)

        inp.send_keys(Keys.RETURN)

    def get_text(self):
        """

        :rtype: str
        """
        self.scroll_to()
        return self.web_element.text

    def scroll_to(self):
        self.driver.execute_script("document.getElementById('{0:s}').scrollIntoView(true)".format(self.id))
        time.sleep(1)

    @property
    def web_element(self):
        """

        :rtype: WebElement
        """
        if self._web_element:
            try:
                self._web_element.get_attribute('id')
            except StaleElementReferenceException:
                self._web_element = None

        if not self._web_element:
            self._web_element = self.driver.find_element(By.CSS_SELECTOR,
                                                         self.SELECTOR.format(self.row, self.col))
        return self._web_element

    @property
    def id(self):
        if not self._id:
            try:
                self._id = self.web_element.get_attribute('id')
            except StaleElementReferenceException:
                self._web_element = None
                self._id = self.web_element.get_attribute('id')
        return self._id


class ProjectEditor(BasePage):
    def add_task(self, number, name, predecessor=None):
        # name
        cell = self._get_task_cell(number)
        # cell.scroll_to()
        cell.fill_cell(name)
        # predecessor
        if predecessor:
            assert isinstance(predecessor, int)
            self._get_predecessor_cell(number).fill_cell(predecessor)

    def _get_task_cell(self, n):
        """

        :param n: task number
        :return: Cell
        """
        TASK_COL = 5
        return Cell(self.driver, n, TASK_COL)

    def _get_predecessor_cell(self, n):
        PREDECESSOR_COL = 9
        return Cell(self.driver, n, PREDECESSOR_COL)

    def save(self):
        time.sleep(1)
        self.driver.find_element(*ProjectEditorLocators.SAVE_BTN).click()
        time.sleep(5)

    def check_task_presence(self, n):
        time.sleep(5)
        text = self._get_task_cell(n).get_text()
        assert len(text) > 0 and not text.isspace(), 'Task {0:d} not found'.format(n)
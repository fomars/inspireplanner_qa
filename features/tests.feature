@smoke
#noinspection CucumberUndefinedStep
Feature: Add, save and open project

  @10 @few
  Scenario: Add a project with 10 tasks
    Given I open inspireplanner as qa_user
    When Create new project named 10t
    Then I create 10 subsequent tasks
    And I save the project
    And I close the project
    And I open project this
    And I check task 10

  @50 @average
  Scenario: Add a project with 50 tasks
    Given I open inspireplanner as qa_user
    When Create new project named 50t
    Then I create 50 subsequent tasks
    And I save the project
    And I close the project
    And I open project this
    And I check task 50

  @100 @many
  Scenario: Add a project with 100 tasks
    Given I open inspireplanner as qa_user
    When Create new project named 100t
    Then I create 100 subsequent tasks
    And I save the project
    And I close the project
    And I open project this
    And I check task 100

  @200 @plenty
  Scenario: Add a project with 200 tasks
    Given I open inspireplanner as qa_user
    When Create new project named 200t
    Then I create 200 subsequent tasks
    And I save the project
    And I close the project
    And I open project this
    And I check task 200


  @open200 @open
  Scenario: Open project with 200 tasks
    Given I open inspireplanner as qa_user
    And I open project 200t
    And I check task 200
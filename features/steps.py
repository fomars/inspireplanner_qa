import time
import datetime
from pages.pageobjects import *
from lettuce import *

__author__ = 'fomars'

Links = world.Links


@step('I open inspireplanner as (\w+)')
def start_work(step, user):
    LoginPage(Links.LOGIN_PAGE, world.driver).login(*world.qa_user)


@step('Create new project named (\w+)')
def create_new_project(step, name):
    name = datetime.datetime.now().strftime('%m_%d_%H_%M') + "_%s" % name
    PlannerTab(Links.PLANNER_TAB, world.driver).new_project(name)
    world.project_name = name


@step('I see project editor with (\w+)')
def check_project_editor(step, name):
    pass


@step('I create (\d+) subsequent tasks')
def create_n_tasks(step, n):
    NAME = 'task{0:d}'
    n = int(n)
    project_editor = ProjectEditor(Links.PROJECT_EDITOR, world.driver)
    project_editor.add_task(1, NAME.format(1))
    for i in range(2, n+1):
        time.sleep(1)
        project_editor.add_task(i, NAME.format(i), i-1)

@step('I save the project')
def save_project(step):
    ProjectEditor(Links.PROJECT_EDITOR, world.driver).save()


@step('I close the project')
def close_project(step):
    PlannerTab(Links.PLANNER_TAB, world.driver).go_home()


@step('I open project (\w+)')
def measure_load_time(step, project_name):
    if project_name == 'this':
        project_name = world.project_name
    PlannerTab(Links.PLANNER_TAB, world.driver).open_project(project_name)


@step('I check task (\d+)')
def check_task_presence(step, n):
    ProjectEditor(Links.PROJECT_EDITOR, world.driver).check_task_presence(int(n))
__author__ = 'fomars'

qa_user = 'qa@inspireplanner.com', 'tester123'

remote = True
browser = 'firefox'

desired_cap = {
    'chrome': {
        'browser': 'Chrome',
        'browser_version': '39.0',
        'os': 'Windows',
        'os_version': '7',
        'resolution': '1366x768',
        'browserstack.local': True},
    'safari': {
        'browser': 'Safari',
        'browser_version': '8.0',
        'os': 'OS X',
        'os_version': 'Yosemite',
        'resolution': '1280x1024',
        'browserstack.local': True},
    'ie': {
        'browser': 'IE',
        'browser_version': '11.0',
        'os': 'Windows',
        'os_version': '7',
        'resolution': '1366x768',
        'browserstack.local': True},
    'firefox': {
        'browser': 'Firefox',
        'browser_version': '35.0',
        'os': 'Windows',
        'os_version': '7',
        'resolution': '1280x800',
        'browserstack.local': True}
}

BIG_PROJECT = 200


class Links():
    PLANNER_TAB = 'https://inspire1.na17.visual.force.com/apex/InspireProject'
    LOGIN_PAGE = 'https://login.salesforce.com'
    PROJECT_EDITOR = ''

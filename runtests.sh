#!/usr/bin/env bash
# arguments - lettuce tags
#export DISPLAY=:1
./BrowserStackLocal -force -forcelocal rMesVmunGdydpzEXuV6R localhost,3000,0 &

sleep 30

echo "Select browser please:"
select browser in "chrome" "firefox" "ie" "safari" ; do
    echo Selected: ${browser}
    sed -i "s/\(browser *= *\).*/\1'$browser'/" config.py
    sed -i '' "s/\(browser *= *\).*/\1'$browser'/" config.py
    break
done

if [ $# -eq 0 ]
  then
    lettuce --with-xunit
else
    tags=""
    for tag in "$@"
    do
        tags="$tags-t $tag "
    done
    echo "$tags"
    eval "lettuce --with-xunit ${tags}"
fi
xmllint --format lettucetests.xml | pygmentize -l xml

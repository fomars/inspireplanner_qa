import config
from selenium import webdriver
from lettuce import after, world, before

__author__ = 'fomars'

world.Links = config.Links
world.qa_user = config.qa_user


@before.all
def setup_browser():
    if config.remote:
        world.driver = webdriver.Remote(
            command_executor='http://thainguyen8:rMesVmunGdydpzEXuV6R@hub.browserstack.com:80/wd/hub',
            desired_capabilities=config.desired_cap.get(config.browser.lower()))
    else:
        world.driver = webdriver.Chrome()
    world.driver.implicitly_wait(10)


@after.all
def close_browser(total):
    world.driver.quit()


@after.each_step
def get_screenshot(step):
    if step.failed:
        world.driver.save_screenshot('%s.png' % step.sentence)

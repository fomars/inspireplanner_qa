before the first run:
 - login to salesforce.com under QA account manually and submit your device from e-mail
 
 access to vm: 
 ssh -i .ssh/inspire.pem ubuntu@ec2-52-24-251-43.us-west-2.compute.amazonaws.com
 
 run with vnc:
    $ vncserver
    $ export DISPLAY=:1
    $ lettuce
    
 run headlessy:
    $ sudo apt-get install xvfb
    $ xvfb-run lettuce

    